package com.teknobli.merchant.notifymicroservice;

public class Endpoints {

    public static final String BASE_URL = "http://localhost:8005/notify";

    public static final String stockUpdateNotification = "/product/updateStock/";
}
